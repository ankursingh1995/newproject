from django.db import models
# Create your models here.

class Candidate(models.Model):
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	company_name = models.CharField(max_length=50)
	age = models.IntegerField()
	city = models.CharField(max_length=20)
	state = models.CharField(max_length=20)
	zipcode = models.CharField(max_length=50)
	email = models.EmailField(max_length=100)
	website = models.URLField(max_length=100)

	def __str__(self):
		return self.first_name + self.last_name