from .models import Candidate
from rest_framework import serializers

class CandidateSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Candidate
		fields = ('id', 'first_name', 'last_name', 'company_name', 'age', 'city', 'zipcode', 'email', 'website')
