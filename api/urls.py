from django.conf.urls import url
#from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/users/', views.CandidateDetailView.as_view()),
    url(r'^delete/(?P<pk>\d+)', views.CandidateDetailView.as_view(), name='delete_event'),
    #url(r'^api/users/<int:pk>/', views.CandidateDetailView.as_view()),
]

#urlpatterns = format_suffix_patterns(urlpatterns)
