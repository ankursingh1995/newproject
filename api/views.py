from django.shortcuts import render
from rest_framework import viewsets
from .models import Candidate
from .serializers import CandidateSerializer
from django.http import HttpResponse
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q

def index(request):
    return HttpResponse("Hello")

class LargeResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 10

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000

class CandidateDetailView(APIView):

	pagination_class = LargeResultsSetPagination
	pagination_class = StandardResultsSetPagination

	def get(self, request):
		
		q = Q ()
		first_name = request.GET.get('first_name', None)
		age = request.GET.get('age', None)
		limit = request.GET.get('limit', None)
		if first_name:
			q = q & Q(first_name=first_name)
		else:
			q = q
		if limit:
			limit = limit
		else:
			limit = 5  
		queryset = Candidate.objects.filter(q).order_by('age')[:limit]
		serializer_class = CandidateSerializer(queryset, many=True)
		
		#queryset = {"Country":[{'cont_id':x.cont_id,'name':x.name,'is_valid':x.is_valid} for x in queryset]}
		
		return Response(serializer_class.data)

	# def delete(self, request):
 #        event = self.get_object(self)
 #        event.delete()
 #        return Response(status=status.HTTP_204_NO_CONTENT
